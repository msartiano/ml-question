# Goal
With the provided dataset, write a classifier that classifies reviews as 'positive' or 'negative.
Then calculate the accuracy score of the predictions.

# Description
You are provided with a training and a test datasets in CSV format.

The training set contains 800 positive and 800 negative media reviews.
The test set contains 200 positive and 200 negative media reviews.

Each dataset has two categories:
- Pos (reviews that express a favourable sentiment)
- Neg (reviews that express an unfavourable sentiment)

For this exercise, we will assume that all reviews are either positive or negative; there are no neutral reviews.

You will need to build a classification model for solving the questions.
